<?php

/**
 * @file
 * Enables modules and site configuration for the highschool profile.
 */

/**
* Implements hook_form_FORM_ID_alter().
*/
function highschool_form_search_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form['advanced']['reset'] = [
    '#type' => 'button',
    '#value' => t('Reset'),
    '#attributes' => ['class' => ['search-form-reset']],
  ];
}
