(function($, Drupal){
  Drupal.behaviors.globalBehavior = {
    attach: function(context, settings) {
      // Close alert banner - Start
      if ($('.block-alert-banner .messages__button', context).length > 0) {
        $('.block-alert-banner .messages__button', context).click(function(){
          $(this).parents('.block-alert-banner').hide();
        });
      }
      // Close alert banner - End
      // Reset advanced search form - Start
      if ($('body.path-search', context).length > 0) {
        $('body.path-search .search-form .search-form-reset', context).on('click', function(e){
          e.preventDefault();
          let form = $(this).parents('.search-form');
          // Reset form field values.
          form.trigger('reset');
          // Edge case for checkboxes.
          form.find('input:checkbox').removeAttr('checked');
          // Submit the reset form.
          form.submit();
        });
      }
      // Reset advanced search form - End
    }
  }
})(jQuery, Drupal);
